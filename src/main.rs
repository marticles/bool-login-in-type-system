fn main() {
    // Evaluating true and false.
    println!(
        "true false true = {}",
        <True as Lambda<(False, True)>>::Output::VALUE
    );
    println!(
        "false false true = {}",
        <True as Lambda<(False, True)>>::Output::VALUE
    );

    // Evaluating not.
    println!("!false = {}", <Not as Lambda<False>>::Output::VALUE);
    println!("!true = {}", <Not as Lambda<True>>::Output::VALUE);

    // Evaluating and.
    println!(
        "false && false = {}",
        <And as Lambda<(False, False)>>::Output::VALUE
    );
    println!(
        "true && false = {}",
        <And as Lambda<(True, False)>>::Output::VALUE
    );
    println!(
        "false && true = {}",
        <And as Lambda<(False, True)>>::Output::VALUE
    );
    println!(
        "true && true = {}",
        <And as Lambda<(True, True)>>::Output::VALUE
    );

    // Evaluating or.
    println!(
        "false || false = {}",
        <Or as Lambda<(False, False)>>::Output::VALUE
    );
    println!(
        "true || false = {}",
        <Or as Lambda<(True, False)>>::Output::VALUE
    );
    println!(
        "false || true = {}",
        <Or as Lambda<(False, True)>>::Output::VALUE
    );
    println!(
        "true || true = {}",
        <Or as Lambda<(True, True)>>::Output::VALUE
    );

    // Complex example.
    println!(
        "!true || false || (!false && true) = {}",
        <Or as Lambda<(
            <Or as Lambda<(<Not as Lambda<True>>::Output, False)>>::Output,
            <And as Lambda<(<Not as Lambda<False>>::Output, True)>>::Output
        )>>::Output::VALUE
    );
}

trait Lambda<T> {
    type Output;
}

struct True;

impl True {
    const VALUE: bool = true;
}

impl<A, B> Lambda<(A, B)> for True {
    type Output = A;
}

struct False;

impl False {
    const VALUE: bool = false;
}

impl<A, B> Lambda<(A, B)> for False {
    type Output = B;
}

struct Not;

impl<T: Lambda<(False, True)>> Lambda<T> for Not {
    type Output = T::Output;
}

struct And;

impl<A: Lambda<(B, False)>, B> Lambda<(A, B)> for And {
    type Output = <A as Lambda<(B, False)>>::Output;
}

struct Or;

impl<A: Lambda<(True, B::Output)>, B: Lambda<(True, False)>> Lambda<(A, B)> for Or {
    type Output = <A as Lambda<(True, B::Output)>>::Output;
}
